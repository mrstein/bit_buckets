<?php
 
//The URL we want to send a HTTP request to.
//In this case, it is a script on my local machine.
$url = 'http://facebook.com';
 
//Initiate cURL
$ch = curl_init($url);
 
//Tell cURL that it should only spend 10 seconds
//trying to connect to the URL in question.
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
 
//A given cURL operation should only take
//30 seconds max.
curl_setopt($ch, CURLOPT_TIMEOUT, 2);
 
//Tell cURL to return the response output as a string.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
//Execute the request.
$response = curl_exec($ch);
 
//Did an error occur? If so, dump it out.
if(curl_errno($ch)){
    throw new Exception(curl_error($ch));
}