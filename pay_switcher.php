<?php
require_once 'koneksi.php';
class emp{}
//API URL
$url = 'http://103.81.248.17:8080/gwlkm/inquiry_new.php';
$no_transaksi   = mysqli_escape_string($db, $_POST['no_transaksi']);//Unik Code
$kode_produk    = mysqli_escape_string($db, $_POST['kode_produk']); //Product Code
$periode        = mysqli_escape_string($db, $_POST['periode']);//Periode
$nasabah_id     = mysqli_escape_string($db, $_POST['nasabah_id']);//Nasabah Id 1
$customer_no    = mysqli_escape_string($db, $_POST['customer_no']);//Nomor Tujuan
$kd_lkm         = mysqli_escape_string($db, $_POST['kd_lkm']);//kd_lembaga
$jenis          = mysqli_escape_string($db, $_POST['jenis']);//jenis inquiry atau bukan jika 032 inquiry dulu jika 033 langsung payment
$pin            = mysqli_escape_string($db, $_POST['pin']);
///////////////////////////////////////////////////////////SEarch ip lkm
$CekLKM = mysqli_query($db, "SELECT * FROM switching WHERE kodeLKM = '$kd_lkm'");
$JumLKM = mysqli_num_rows($CekLKM);
if ($JumLKM=='1'){
    $Link   = mysqli_fetch_array($CekLKM);
    $SendTo = $Link['ip_address'].":".$Link['port'];
    $urlSaldoPay    = 'http://'.$SendTo.'/gwlkm/getSaldoPay.php';
    $chSaldoPay     = curl_init($urlSaldoPay);
    $dataSaldoPay   = array(
        'id'     => $nasabah_id,
            'pin'    => $pin
    );
    $payload = json_encode(array("user" => $dataSaldoPay));
    /////////////////////////////////////SEND TO GETSALDOPAY//////////////////////////////////////////////////////
    curl_setopt($chSaldoPay, CURLOPT_POSTFIELDS, $dataSaldoPay);
    curl_setopt($chSaldoPay, CURLOPT_RETURNTRANSFER, true);
    $result     = curl_exec($chSaldoPay);
    curl_close($chSaldoPay);
     $dataSaldoPay = json_decode($result, true);
    /////////////////////////////////////END TO GETSALDOPAY//////////////////////////////////////////////////////
    /////////////////////////////////////RESPONSE FROM GETSALDO PAY//////////////////////////////////////////////
    $statusSaldoPay = $dataSaldoPay['success'];
    if ($statusSaldoPay=='1') {
        $norek = $dataSaldoPay['norek'];
        $saldo = $dataSaldoPay['saldo'];
        //cek produk
        $CProduk = mysqli_query($db, "SELECT * FROM product_config WHERE kodeLKM = '$kd_lkm' AND kode_product = '$kode_produk'");
        $dProduk = mysqli_fetch_array($CProduk);
        if (empty($dProduk)) {
            $CProduk = mysqli_query($db, "SELECT * FROM product_config WHERE kodeLKM = '0000' AND kode_product = '$kode_produk'");
            $dProduk = mysqli_fetch_array($CProduk);
            if (empty($dProduk)) {
                $response = new emp();
                $response->success = 0;
                $response->message = "Produk belum di konfigurasi";
                $response->saldo = $amount;
                die(json_encode($response));
            }else{
                $kdBiller = $dProduk['kode_biller'];
                $nmProduk = $dProduk['desc'];
                $HrgDasar = $dProduk['harga_dasar'];
                $harga_jual = $dProduk['harga_jual'];
                $profAggr   = $dProduk['fee_sbk'];
                $jenisAdm   = $dProduk['jenisAdm'];
                $AdmTag   = $dProduk['AdmTag'];
                if ($jenisAdm=='include') {
                    $amount   = $harga_jual;
                    $Adm      = $amount-$HrgDasar-$profAggr;
                }else{
                    $amount   = $harga_jual;
                    $Adm      = abs($AdmTag-$profAggr);
                }
                $urlLKM   = $url;
                if ((int)$saldo<(int)$amount) {
                    $response = new emp();
                    $response->success = 0;
                    $response->message = "Saldo Anda Tidak Mencukupi";
                    $response->saldo = $amount;
                    die(json_encode($response));
                }else{
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $urlReceiveAuth    = 'http://ussisbk.com/fin_lkm/ReceiveAuth.php';
                    $chReceiveAuth     = curl_init($urlReceiveAuth);
                    $dataAuth          = array(
                        'kd_lkm'    => $kd_lkm,
                            'ket'       => 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no,
                                'produk'    => $kode_produk,
                                    'nominal'   => $HrgDasar,
                                        'adm'       => $Adm
                    );
                    $payload = json_encode(array("user" => $dataAuth));
                    curl_setopt($chReceiveAuth, CURLOPT_POSTFIELDS, $dataAuth);
                    curl_setopt($chReceiveAuth, CURLOPT_RETURNTRANSFER, true);
                    $resultAuth = curl_exec($chReceiveAuth);
                    curl_close($chReceiveAuth);
                    $dataAuth = json_decode($resultAuth, true);
                     ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $statusAuth = $dataAuth['successApex'];
                    if ($statusAuth=='1') {
                        $urlbill    = 'http://103.81.248.18:8080/gwlkm/buy.php';
                        $chBuy      = curl_init($urlbill);
                        $databill   = array(
                            'customer_no'    => $customer_no,
                            'kode_produk'     => $kode_produk,
                            'no_transaksi'    => $no_transaksi
                        );
                        $payload = json_encode(array("user" => $databill));
                        curl_setopt($chBuy, CURLOPT_POSTFIELDS, $databill);
                        curl_setopt($chBuy, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($chBuy, CURLOPT_NOSIGNAL, 1);
                        ///curl_setopt($chBuy, CURLOPT_TIMEOUT_MS, '20000L');
                        $resultBill       = curl_exec($chBuy);
                        $curl_errno_Trans = curl_errno($chBuy);
                        $curl_error_Trans = curl_error($chBuy);
                        curl_close($chBuy);
                        $databill = json_decode($resultBill, true);
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ($curl_errno_Trans > 0) {
                            $response = new emp();
                            $response->success = 0;
                            $response->message = "Koneksi bermasalah";
                            $response->response_biller=$databill;
                            $response->saldo = $amount;
                            die(json_encode($response));
                        }else{
                            $status = $databill['rc'];
                            if ($status=='00') {
                                ////////////////////////////////////////////////////////////////////////////////////////
                                $query = "SELECT max(NoTrans) as maxKode FROM history";
                                $hasil = mysqli_query($db,$query);
                                $data  = mysqli_fetch_array($hasil);
                                $kodeTrans = $data['maxKode'];
                                $noUrut = (int) substr($kodeTrans, 3, 10);
                                $noUrut++;
                                $char = "TRX";
                                //param db
                                $kodeTrans = $char . sprintf("%010s", $noUrut);
                                $DateTrans = date('Y-m-d');
                                $TimeTrans = date('H:i:s');
                                $Keterangan = 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no;
                                $InsHistory = mysqli_query($db, "INSERT INTO history VALUES ('$kodeTrans', '$nasabah_id','$DateTrans', '$TimeTrans', '$kd_lkm', '$Keterangan', '$kode_produk', '$amount', '$Adm', '0', 'unix', '', 'reff')");
                                $InFeeSbk   = mysqli_query($db, "INSERT INTO fee VALUES ('', '$DateTrans', '$TimeTrans', '$nasabah_id', '$kd_lkm', '$profAggr', '$Keterangan')");
                        ///////////////////////////////////////////////////////////////////////////////////////////////
                                $url        = 'http://'.$SendTo.'/gwlkm/PayHandler.php';
                                //$url    = 'http://localhost/gwlkm/PayHandler.php';
                                $ch     = curl_init($url);
                                $data   = array(
                                    'tujuan'    => $customer_no,
                                        'nasabahid'  => $nasabah_id,
                                            'nominal'   => $amount,
                                                'adm'       => $Adm,
                                                    'reff_id'   => $no_transaksi
                                );
                                $payload = json_encode(array("user" => $data));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $result = curl_exec($ch);
                                curl_close($ch);
                                $data = json_decode($result, true);
                                $status = $data['success'];
                                    if ($status=='1') {
                                        $url    = 'http://ussisbk.com/fin_lkm/ReceiveTrans.php';
                                        $ch     = curl_init($url);
                                        $dataApex   = array(
                                            'kd_lkm'    => $kd_lkm,
                                                'ket'       => 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no,
                                                    'produk'    => $kode_produk,
                                                        'nominal'   => $amount,
                                                            'adm'       => $Adm
                                        );
                                        $payload = json_encode(array("user" => $dataApex));
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataApex);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $dataApex = json_decode($result, true);
                                        $response = new emp();
                                        $response->success = 1;
                                        $response->message="Berhasil membeli produk";
                                        $response->response_biller = $databill;
                                        $response->saldo = $amount;
                                        echo json_encode($response,JSON_PRETTY_PRINT); 
                                        ////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    }else{
                                         $response = new emp();
                                         $response->success = 1;
                                         $response->message="Berhasil membeli produk";
                                         $response->saldo = $amount;
                                         $response->response_biller = $databill;
                                         echo json_encode($response,JSON_PRETTY_PRINT); 
                                    }
                            }elseif($status=='02'){
                                 ////////////////////////////////////////////////////////////////////////////////////////
                                $query = "SELECT max(NoTrans) as maxKode FROM history";
                                $hasil = mysqli_query($db,$query);
                                $data  = mysqli_fetch_array($hasil);
                                $kodeTrans = $data['maxKode'];
                                $noUrut = (int) substr($kodeTrans, 3, 10);
                                $noUrut++;
                                $char = "TRX";
                                //param db
                                $kodeTrans = $char . sprintf("%010s", $noUrut);
                                $DateTrans = date('Y-m-d');
                                $TimeTrans = date('H:i:s');
                                $Keterangan = 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no;
                                $InsHistory = mysqli_query($db, "INSERT INTO history VALUES ('$kodeTrans', '$nasabah_id','$DateTrans', '$TimeTrans', '$kd_lkm', '$Keterangan', '$kode_produk', '$amount', '$Adm', '2')");
                                $InFeeSbk   = mysqli_query($db, "INSERT INTO fee VALUES ('', '$DateTrans', '$TimeTrans', '$nasabah_id', '$kd_lkm', '$profAggr', '$Keterangan')");
                        ///////////////////////////////////////////////////////////////////////////////////////////////
                                $response = new emp();
                                $response->success = 2;
                                $response->message = "Transaksi gagal karena gangguan";
                                $response->saldo = $amount;
                                $response->response_biller = $databill;
                                echo json_encode($response,JSON_PRETTY_PRINT); 
                            }else{
                                 ////////////////////////////////////////////////////////////////////////////////////////
                                $query = "SELECT max(NoTrans) as maxKode FROM history";
                                $hasil = mysqli_query($db,$query);
                                $data  = mysqli_fetch_array($hasil);
                                $kodeTrans = $data['maxKode'];
                                $noUrut = (int) substr($kodeTrans, 3, 10);
                                $noUrut++;
                                $char = "TRX";
                                //param db
                                $kodeTrans = $char . sprintf("%010s", $noUrut);
                                $DateTrans = date('Y-m-d');
                                $TimeTrans = date('H:i:s');
                                $Keterangan = 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no;
                                $InsHistory = mysqli_query($db, "INSERT INTO history VALUES ('$kodeTrans', '$nasabah_id','$DateTrans', '$TimeTrans', '$kd_lkm', '$Keterangan', '$kode_produk', '$amount', '$Adm', '1')");
                                $InFeeSbk   = mysqli_query($db, "INSERT INTO fee VALUES ('', '$DateTrans', '$TimeTrans', '$nasabah_id', '$kd_lkm', '$profAggr', '$Keterangan')");
                        ///////////////////////////////////////////////////////////////////////////////////////////////
                                $url        = 'http://'.$SendTo.'/gwlkm/PayHandler.php';
                                //$url    = 'http://localhost/gwlkm/PayHandler.php';
                                $ch     = curl_init($url);
                                $data   = array(
                                    'tujuan'    => $customer_no,
                                        'nasabahid'  => $nasabah_id,
                                            'nominal'   => $amount,
                                                'adm'       => $Adm,
                                                    'reff_id'   => $no_transaksi
                                );
                                $payload = json_encode(array("user" => $data));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $result = curl_exec($ch);
                                curl_close($ch);
                                $data = json_decode($result, true);
                                $status = $data['success'];
                        
                                        $url    = 'http://ussisbk.com/fin_lkm/ReceiveTrans.php';
                                        $ch     = curl_init($url);
                                        $dataApex   = array(
                                            'kd_lkm'    => $kd_lkm,
                                                'ket'       => 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no,
                                                    'produk'    => $kode_produk,
                                                        'nominal'   => $amount,
                                                            'adm'       => $Adm
                                        );
                                        $payload = json_encode(array("user" => $dataApex));
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataApex);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $dataApex = json_decode($result, true);
                                 $response = new emp();
                                 $response->success = 3;
                                 $response->message = "Transaksi Sedang Diproses ...";
                                 $response->response_biller=$databill;
                                 echo json_encode($response,JSON_PRETTY_PRINT); 
                            }
                        }
                    }else{
                        echo "$resultAuth";
                    }
                }
            }
        }else{
            $kdBiller = $dProduk['kode_biller'];
            $nmProduk = $dProduk['desc'];
            $HrgDasar = $dProduk['harga_dasar'];
            $harga_jual = $dProduk['harga_jual'];
            $profAggr   = $dProduk['fee_sbk'];
            $jenisAdm   = $dProduk['jenisAdm'];
            $AdmTag   = $dProduk['AdmTag'];
            if ($jenisAdm=='include') {
                $amount   = $harga_jual;
                $Adm      = $amount-$HrgDasar-$profAggr;
            }else{
                $amount   = $harga_jual;
                $Adm      = abs($AdmTag-$profAggr);
            }
            $urlLKM   = $url;
            if ((int)$saldo<(int)$amount) {
                $response = new emp();
                $response->success = 0;
                $response->message = "Saldo Anda Tidak Mencukupi";
                $response->saldo = $amount;
                die(json_encode($response));
            }else{
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                $urlReceiveAuth    = 'http://ussisbk.com/fin_lkm/ReceiveAuth.php';
                $chReceiveAuth     = curl_init($urlReceiveAuth);
                $dataAuth          = array(
                    'kd_lkm'    => $kd_lkm,
                        'ket'       => 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no,
                            'produk'    => $kode_produk,
                                'nominal'   => $HrgDasar,
                                    'adm'       => $Adm
                );
                $payload = json_encode(array("user" => $dataAuth));
                curl_setopt($chReceiveAuth, CURLOPT_POSTFIELDS, $dataAuth);
                curl_setopt($chReceiveAuth, CURLOPT_RETURNTRANSFER, true);
                $resultAuth = curl_exec($chReceiveAuth);
                curl_close($chReceiveAuth);
                $dataAuth = json_decode($resultAuth, true);
                 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                $statusAuth = $dataAuth['successApex'];
                if ($statusAuth=='1') {
                    $urlbill    = 'http://103.81.248.18:8080/gwlkm/buy.php';
                    $chBuy      = curl_init($urlbill);
                    $databill   = array(
                        'customer_no'    => $customer_no,
                        'kode_produk'     => $kode_produk,
                        'no_transaksi'    => $no_transaksi
                    );
                    $payload = json_encode(array("user" => $databill));
                    curl_setopt($chBuy, CURLOPT_POSTFIELDS, $databill);
                    curl_setopt($chBuy, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($chBuy, CURLOPT_NOSIGNAL, 1);
                    ///curl_setopt($chBuy, CURLOPT_TIMEOUT_MS, '20000L');
                    $resultBill       = curl_exec($chBuy);
                    $curl_errno_Trans = curl_errno($chBuy);
                    $curl_error_Trans = curl_error($chBuy);
                    curl_close($chBuy);
                    $databill = json_decode($resultBill, true);
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if ($curl_errno_Trans > 0) {
                        $response = new emp();
                        $response->success = 0;
                        $response->message = "Koneksi bermasalah";
                        $response->response_biller=$databill;
                        $response->saldo = $amount;
                        die(json_encode($response));
                    }else{
                        $status = $databill['rc'];
                        if ($status=='00') {
                            ////////////////////////////////////////////////////////////////////////////////////////
                            $query = "SELECT max(NoTrans) as maxKode FROM history";
                            $hasil = mysqli_query($db,$query);
                            $data  = mysqli_fetch_array($hasil);
                            $kodeTrans = $data['maxKode'];
                            $noUrut = (int) substr($kodeTrans, 3, 10);
                            $noUrut++;
                            $char = "TRX";
                            //param db
                            $kodeTrans = $char . sprintf("%010s", $noUrut);
                            $DateTrans = date('Y-m-d');
                            $TimeTrans = date('H:i:s');
                            $Keterangan = 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no;
                            $InsHistory = mysqli_query($db, "INSERT INTO history VALUES ('$kodeTrans', '$nasabah_id','$DateTrans', '$TimeTrans', '$kd_lkm', '$Keterangan', '$kode_produk', '$amount', '$Adm', '0')");
                            $InFeeSbk   = mysqli_query($db, "INSERT INTO fee VALUES ('', '$DateTrans', '$TimeTrans', '$nasabah_id', '$kd_lkm', '$profAggr', '$Keterangan')");
                    ///////////////////////////////////////////////////////////////////////////////////////////////
                            $url        = 'http://'.$SendTo.'/gwlkm/PayHandler.php';
                            //$url    = 'http://localhost/gwlkm/PayHandler.php';
                            $ch     = curl_init($url);
                            $data   = array(
                                'tujuan'    => $customer_no,
                                    'nasabahid'  => $nasabah_id,
                                        'nominal'   => $amount,
                                            'adm'       => $Adm,
                                                'reff_id'   => $no_transaksi
                            );
                            $payload = json_encode(array("user" => $data));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $data = json_decode($result, true);
                            $status = $data['success'];
                                if ($status=='1') {
                                    $url    = 'http://ussisbk.com/fin_lkm/ReceiveTrans.php';
                                    $ch     = curl_init($url);
                                    $dataApex   = array(
                                        'kd_lkm'    => $kd_lkm,
                                            'ket'       => 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no,
                                                'produk'    => $kode_produk,
                                                    'nominal'   => $amount,
                                                        'adm'       => $Adm
                                    );
                                    $payload = json_encode(array("user" => $dataApex));
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataApex);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $result = curl_exec($ch);
                                    curl_close($ch);
                                    $dataApex = json_decode($result, true);
                                    $response = new emp();
                                    $response->success = 1;
                                    $response->message="Berhasil membeli produk";
                                    $response->response_biller = $databill;
                                    $response->saldo = $amount;
                                    echo json_encode($response,JSON_PRETTY_PRINT); 
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////
                                }else{
                                     $response = new emp();
                                     $response->success = 1;
                                     $response->message="Berhasil membeli produk";
                                     $response->saldo = $amount;
                                     $response->response_biller = $databill;
                                     echo json_encode($response,JSON_PRETTY_PRINT); 
                                }
                        }elseif($status=='02'){
                             ////////////////////////////////////////////////////////////////////////////////////////
                            $query = "SELECT max(NoTrans) as maxKode FROM history";
                            $hasil = mysqli_query($db,$query);
                            $data  = mysqli_fetch_array($hasil);
                            $kodeTrans = $data['maxKode'];
                            $noUrut = (int) substr($kodeTrans, 3, 10);
                            $noUrut++;
                            $char = "TRX";
                            //param db
                            $kodeTrans = $char . sprintf("%010s", $noUrut);
                            $DateTrans = date('Y-m-d');
                            $TimeTrans = date('H:i:s');
                            $Keterangan = 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no;
                            $InsHistory = mysqli_query($db, "INSERT INTO history VALUES ('$kodeTrans', '$nasabah_id','$DateTrans', '$TimeTrans', '$kd_lkm', '$Keterangan', '$kode_produk', '$amount', '$Adm', '2')");
                            $InFeeSbk   = mysqli_query($db, "INSERT INTO fee VALUES ('', '$DateTrans', '$TimeTrans', '$nasabah_id', '$kd_lkm', '$profAggr', '$Keterangan')");
                    ///////////////////////////////////////////////////////////////////////////////////////////////
                            $response = new emp();
                            $response->success = 2;
                            $response->message = "Transaksi gagal karena gangguan";
                            $response->saldo = $amount;
                            $response->response_biller = $databill;
                            echo json_encode($response,JSON_PRETTY_PRINT); 
                        }else{
                             ////////////////////////////////////////////////////////////////////////////////////////
                            $query = "SELECT max(NoTrans) as maxKode FROM history";
                            $hasil = mysqli_query($db,$query);
                            $data  = mysqli_fetch_array($hasil);
                            $kodeTrans = $data['maxKode'];
                            $noUrut = (int) substr($kodeTrans, 3, 10);
                            $noUrut++;
                            $char = "TRX";
                            //param db
                            $kodeTrans = $char . sprintf("%010s", $noUrut);
                            $DateTrans = date('Y-m-d');
                            $TimeTrans = date('H:i:s');
                            $Keterangan = 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no;
                            $InsHistory = mysqli_query($db, "INSERT INTO history VALUES ('$kodeTrans', '$nasabah_id','$DateTrans', '$TimeTrans', '$kd_lkm', '$Keterangan', '$kode_produk', '$amount', '$Adm', '1')");
                            $InFeeSbk   = mysqli_query($db, "INSERT INTO fee VALUES ('', '$DateTrans', '$TimeTrans', '$nasabah_id', '$kd_lkm', '$profAggr', '$Keterangan')");
                    ///////////////////////////////////////////////////////////////////////////////////////////////
                            $url        = 'http://'.$SendTo.'/gwlkm/PayHandler.php';
                            //$url    = 'http://localhost/gwlkm/PayHandler.php';
                            $ch     = curl_init($url);
                            $data   = array(
                                'tujuan'    => $customer_no,
                                    'nasabahid'  => $nasabah_id,
                                        'nominal'   => $amount,
                                            'adm'       => $Adm,
                                                'reff_id'   => $no_transaksi
                            );
                            $payload = json_encode(array("user" => $data));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $data = json_decode($result, true);
                            $status = $data['success'];
                    
                                    $url    = 'http://ussisbk.com/fin_lkm/ReceiveTrans.php';
                                    $ch     = curl_init($url);
                                    $dataApex   = array(
                                        'kd_lkm'    => $kd_lkm,
                                            'ket'       => 'Transaksi '.$nmProduk.' ke id pel:'.$customer_no,
                                                'produk'    => $kode_produk,
                                                    'nominal'   => $amount,
                                                        'adm'       => $Adm
                                    );
                                    $payload = json_encode(array("user" => $dataApex));
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataApex);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $result = curl_exec($ch);
                                    curl_close($ch);
                                    $dataApex = json_decode($result, true);
                             $response = new emp();
                             $response->success = 3;
                             $response->message = "Transaksi Sedang Diproses ...";
                             $response->response_biller=$databill;
                             echo json_encode($response,JSON_PRETTY_PRINT); 
                        }
                    }
                }else{
                    echo "$resultAuth";
                }
            }
        }
    }else{
        echo "$result";
    }    
}else{
    $response = new emp();
    $response->success = 0;
    $response->message = "Institusi Tidak Terdaftar";
       die(json_encode($response));
    }